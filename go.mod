module nginx

go 1.19

// replace gitlab.com/mezigar/go-kata-course2 => /home/mezigar/go/src/gitlab.com/mezigar/go-kata-course-2
replace gitlab.com/mezigar/go-kata-course2 => ../go-kata-course-2

require (
	github.com/go-chi/chi/v5 v5.0.8
)

require (
	github.com/PuerkitoBio/goquery v1.8.1 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/tebeka/selenium v0.9.9 // indirect
	golang.org/x/net v0.10.0 // indirect
)
